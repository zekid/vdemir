---
layout: page
title: Tîn
description: Sûre
excerpt: "95'nci Sûre"
modified: 2017-08-29T17:50:00.564948 17:00
encoding: UTF-8
tag: 
 - Tîn
---

## 95. Tîn Sûresi
**(1)** Andolsun incire ve zeytine
**(2)** ve Sina dağına
**(3)** ve bu güvenli şehre (andolsun)
**(4)** andolsun biz insanı en güzel biçimde yarattık
**(5)** sonra onu aşağıların aşağısına çevirdik
**(6)** yalnız inanan(lar) kimseler ve iyi işler yapanlar hariç onlar için kesintisi olmayan bir mükafat vardır
**(7)** bundan sonra dini sana yalanlatan nedir?
**(8)** Allah hüküm verenlerin en iyi hüküm vereni değil midir?

![95]({{ site.baseurl }}/assets/images/ayrac-muhur.png)
