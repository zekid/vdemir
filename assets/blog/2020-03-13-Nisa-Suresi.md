---
layout: page
title: Nisâ
description: Sûre
excerpt: "4'nci Sûre"
modified: 2020-03-13T17:50:00.564948 17:00
encoding: UTF-8
tag: 
 - Nisâ
---

## 4. Nisâ Sûresi

**(1)** ey insanlar Rabbinizden korkun o ki sizi bir tek bir nefisten yarattı ve ondan eşini yarattı ve ikisinden birçok erkekler ve kadınlar üretti ve o ki adına birbirinizden dilekte bulunduğunuz Allah’tan ve akrabalık(bağlarını kırmak)tan sakının şüphesiz Allah sizin üzerinizde gözetleyicidir
**(2)** ve öksüzlere [yetimlere] mallarını verin [habis] pis olanı temiz olanla değiştirmeyin onların mallarını sizin mallarınıza katarak yemeyin çünkü bu büyük bir günahtır
**(3)** şayet öksüz(kızlar) hakkında adaleti sağlayamıyacağınızdan korkarsanız size helal olan kadınlardan ikişer ve üçer ve dörder alın yine adalet yapamayacağınızdan korkarsanız bir tane (alın) yahut ellerinizin sahip olduğu şeyle (yetinin) haksızlık etmemeniz için en uygun olan budur
**(4)** ve bir hak olarak kadınlara mehirlerini verin eğer kendi istekleriyle ondan bir kısmını size bağışlarlarsa iç huzuruyla afiyetle onu yeyin
**(5)** mallarınızı aklı ermezlere vermeyin ki Allah sizin için bir geçim kaynağı yapmıştır ve onunla onları besleyin ve giydirin ve onlara güzel söz söyleyin
**(6)** nikah (çağına) varıncaya kadar öksüzleri deneyin eğer onlarda bir olgunluk görürseniz mallarını hemen kendilerine verin ve tez elden büyüyüp (geri alacaklar) diye israf ile yemeğe kalkmayın ve zengin olan kimse çekinsin ve yoksul olan kimse de uygun şekilde yesin onlara mallarını geri verdiğiniz zaman da yanlarında şahid bulundurun hesapçı olarak Allah yeter
**(7)** ana babanın ve akrabanın geriye bıraktıkları şeylerden bir pay erkeklere vardır ve ana babanın ve akrabanın geriye bıraktıkları şeylerden bir pay kadınlara vardır az olandan veya çoğundan ondan bir hisse ayrılmıştır
**(8)** akrabalar ve öksüzler ve yoksullar (miras) taksim(in)de ne zaman hazır bulunursa ondan onları rızıklandırın ve onlara güzel söz söyleyin
**(9)** şayet kaygı duyanlar arkalarında güçsüz çocuklar bırakırlarsa onların durumundan çekinsinler Allah’tan korksunlar ve doğru söz söylesinler
**(10)** şüphesiz zulüm ile öksüzlerin mallarını yiyen(ler) [yiyen] kimseler doğrusu karınlarına ateş yemektedirler ve çılgın bir ateşe gireceklerdir
**(11)** Allah size çocuklarınız(ın alacağı miras) hakkında tavsiye eder ne bıraktıysa iki kadının payı kadar erkeğe ikiden fazla kadın iseler eğer üçte ikisi onlarındır ve (çocuk) yalnız bir kadın ise eğer (mirasın) yarısı onundur eğer onun (ölenin) çocuğu varsa bıraktığı mirasta ana babasından her birinin altıda bir hissesi vardır eğer onun çocuğu yok da ve ana babası ona varis oluyorsa anasına üçte bir düşer eğer onun kardeşleri varsa anasının payı altıda birdir (bu hükümler) yapacağı vasiyyetten ya da borcundan sonradır babalarınız ve oğullarınızdan hangisinin fayda bakımından size daha yakın olduğunu bilmezsiniz bunlar Allah tarafından koyulmuş haklardır şüphesiz Allah  [Alim Hakim] bilendir hikmet sahibidir
**(12)** eğer onların çocukları yoksa eşlerinizin bıraktıkları mirasın yarısı sizindir eğer onların çocukları varsa ondan yapacakları vasiyyetten veya borçtan sonra bıraktıklarının dörtte biri sizindir eğer sizin de çocuğunuz yoksa bıraktığınızın dörtte biri onlarındır eğer sizin çocuğunuz varsa ondan yapacağınız vasiyyet veya borçtan sonra bıraktığınızın sekizde biri onlarındır eğer erkeğin miras bırakan evladı ve ana babası olmayıp veya kadının bir erkek veya bir kızkardeşi [var] ise onlardan her birine altıda bir düşer eğer onlar bundan fazla iseler üçte bire ortaktırlar ondan yapılan vasiyyetten veya borçtan sonradır Allah'tan zarar verici olmayan vasiyyettir[Alim Halim]  Allah  bilendir halimdir
**(13)** bunlar Allah’ın sınırlarıdır Allah’a ve Elçisine kim ita’at ederse (Allah onu) içinde sürekli kalacakları altlarından ırmaklar akan cennetlere sokar büyük başarı işte budur 
**(14)** ve kim Allah’a ve Elçisi’ne karşı gelir ve O’nun sınırlarını aşarsa (Allah onu) alçaltıcı bir azab içinde sürekli kalacağı ateşe sokar
**(15)** ve kadınlarınızdan fuhuş yapanlar kimseler onlara karşı içinizden dört şahid getirin eğer onlar şahidlik ederlerse ölüm o kadınları alıncaya ya da Allah onların yararına bir yol gösterinceye kadar evlerde tutun (dışarı çıkarmayın)
**(16)** içinizden iki kişi fuhuş yaparsa onlara eziyet edin eğer tevbe eder ve uslanırlarsa artık onlardan vazgeçin çünkü [Tevvab Rahim] Allah tevbeleri çok kabul edendir çok esirgeyendir
**(17)** şu kimselerin Allah’a göre şüphesiz tevbesi makbuldür cahillikle bir kötülük yaparlar sonra hemen ardından dönerler (tevbe ederler)	işte Allah onların tevbesini kabul eder Allah bilendir hüküm ve hikmet sahibidir
**(18)** kötülükler yapan(ların) kimselerin tevbesi (geçerli) değildir nihayet ölüm kendilerine gelip çattığı zaman muhakkak ben şimdi tevbe ettim der ve kafir olarak ölenlere [o] kimselerin (değildir) işte onlar için acı bir azab hazırlamışızdır
**(19)** ey [imanlı] kimseler inanan(lar) kadınları zorla miras yoluyla almanız size helal değildir edepsizlik yapmaları dışında onlara verdiğiniz şeylerin bir kısmını alıp götürmek için onları sıkıştırmayın ve onlarla iyi geçinin eğer onlardan hoşlanmazsanız bilinki Allah sizin hoşlanmadığınız bir şeye ona çok hayır koymuş olabilir 
**(20)** eğer bir eşin yerine başka bir eş almak isterseniz onlardan birine kantarlarca (mal) vermiş olsanız (dahi) ondan hiçbir şeyi (verdiğinizden) geri almayın iftira ederek ve açıkça günaha girerek verdiğinizi alacak mısınız?
**(21)** ve andolsun bazınız bazınıza geçmiş(içli dışlı olmuş)ken nasıl onu alırsınız ve onlar sizden sağlam te’minat almışlardı
**(22)** geçmişte olanlar hariç babalarınızın evlendiği kadınlarla artık evlenmeyin çünkü bu edepsizliktir ve (Allah’ın) hışm(ı)dır ve iğrenç bir yoldur
**(23)** analarınız ve kızlarınız ve kızkardeşleriniz ve halalarınız ve teyzeleriniz ve kardeş kızları ve kızkardeş kızları ve sizi emziren analarınız ve süt bacılarınız ve akarılarınızın naları ve birleştiğiniz karılarınızdan olan evlerinizde bulunan üvey kızlarınız eğer onlarla birleşmeniz olmamışsa üzerinize bir günah yoktur ve kendi sulbünüzden oğullarınızın karıları ve iki kızkardeşi bir arada almanız size haram kılındı ancak geçmişte olanlar hariç şüphesiz [Ğafûr Rahîm] Allah çok bağışlayan çok esirgeyendir
**(24)** ve ellerinize geçen(cariye)ler dışında kadınlardan evli olanlar (haramdır) Allah’ın size yazdığı(yasaklar)dır ve bunlardan ötesi mallarınızla iffetli yaşamak zina etmemek istemeniz size helal kılındı onlardan yararlanmanıza karşılık onlara kesilen ücretlerini bir hak olarak verin hakkın kesiminden sonra karşılıklı anlaşmanız hakkında üzerinize bir günah yoktur şüphesiz [Alîm Hakîm] Allah bilendir hüküm ve hikmet sahibidir
**(25)** ve inanmış hür kadınlarla evlenmek için mali güce içinizden gücü yetmeyen kimse ellerinizde sahip olduğunuz inanmış genç kızlarınızdan (alsın) sizin imanınızı Allah daha iyi bilir hepiniz birbirinizdensiniz öyle ise onlarla ailelerinin izniyle evlenin ve evli iken (gizli) dost edinmemeleri ve zina etmemeleri iffetli yaşamaları [için] güzelce ücretlerini (mehirlerini) verin eğer fuhuş yaparlarsa onlara hür kadınlar üzerine yapılan işkencenin yarısı (uygulanır) bu (cariye ile evlenme) içinizden sıkıntıya düşmekten korkanlar içindir fakat sabretmeniz sizin için daha iyidir [Ğafur Rahim] Allah bağışlayandır esirgeyendir
**(26)** Allah size açıklamak ve sizden önceki(lerin) [evvelki] kimselerin yasalarına sizi iletmek ve günahlarınızı bağışlamak istiyor Allah bilendir hüküm ve hikmet sahibidir
**(27)** Allah sizin tevbenizi kabul etmek istiyor ve şehvetlerine uyan(lar) [o] kimseler sizin büyük bir sapıklığa düşmenizi istiyorlar
**(28)** Allah [telaşeyi kolayca] sizden hafifletmek istiyor ve insan zayıf yaratılmıştır
**(29)** ey inanan(lar) [imanlı] kimseler kendi rızanızla yaptığınız ticaret olan haricinde batılla (haksız yere) aranızda mallarınızı yemeyin canlarınızı öldürmeyin doğrusu [Rahîm] Allah size karşı çok merhametlidir 
**(30)** düşmanlık ile ve zulüm ile kim bunu yaparsa (bilsin ki) onu cehenneme sokacağız ve bu Allah’a karşı kolaydır
**(31)** ne ki ondan size yasaklanan büyük günahlardan eğer kaçınırsanız sizin küçük günahlarınızı örteriz ve sizi güzel bir yere sokarız
**(32)** Allah’ın bir kısmınızı onunla diğerine karşı üstün kıldığı şeylere göz dikmeyin kazandıkları şeylerden erkeklere bir pay vardır ve kazandıkları şeylerden bir pay kadınlara vardır  Allah’ın lutfundan isteyin kuşkusuz Allah her şeyi bilendir
**(33)** ve ana babanın ve akrabanın bıraktıklarından her birine varisler kıldık ve yeminlerinizin bağladığı kimselere hisselerini verin şüphesiz Allah her şeyi üzerine şahittir
**(34)** erkekler kadınlar üzerinde yöneticidirler zira Allah diğerinin üzerine bir kısmını üstün kılmıştır ve çünkü malları ndan infak ederler iyi kadınlar ita’atkar olup Allah’ın kendilerini korumasına karşılık gizliyi korurlar hırçınlık etmelerinden korktuğunuz kadınlara öğüt verin yataklarda onlara sokulmayın ve onları [evlerden çıkartın] eğer size ita’at ederlerse onların aleyhine başka bir yol artık aramayın çünkü Allah yücedir büyüktür
**(35)** eğer aralarının açılmasından endişe duyarsanız erkeğin ailesinden bir hakem ve kadının ailesinden bir hakem gönderin eğer uzlaştırmak isterlerse Allah onların arasını bulur çünkü [Alim Habir] Allah (herşeyi) bilendir haber alandır
**(36)** ve Allah’a kulluk edin O’na hiçbir şeyi ortak koşmayın ve ana babaya ve akrabaya ve öksüzlere ve yoksullara ve yakın komşuya ve uzak komşuya ve yan(ınız)daki arkadaşa ve yolcuya ve ellerinizin altında bulunanlara iyilik edin şüphesiz Allah böbürlenen kurumlu kimselerin sevmez
**(37)** bunlar cimrilik ederler ve cimriliği insanlara emrederler ve Allah’ın bol hazinesinden kendilerine verdiği şeyi gizlerler (biz de) [kafir] inkarcılar için alçaltıcı bir azab hazırlamışızdır
**(38)** bunlar insanlara gösteriş için mallarını verirler Allah’a ve ahiret gününe inanmazlar kimin ise o(nun) arkadaşı şeytan ne kötü bir arkadaş(ı var)dır
**(39)** onlara ne olurdu sanki Allah’a ve ahiret gününe inansalardı ve Allah’ın kendilerine verdiği rızıktan  harcasalardı ve Allah onları biliyor idi
**(40)** şüphesiz Allah zerre kadar haksızlık etmez eğer (zerre miktarı) bir iyilik olsa onu kat kat yapar ve kendi katından büyük bir mükafat verir
**(41)** her ümmetten bir şahid getirdiğimiz zaman ve seni de bunlar üzerine şahid olarak getirdiğimizde (halleri) nice olur?
**(42)** o gün inkar eden(ler) [kafir] kimseler ve Elçi’ye karşı gelenler (mümkün olsa) yer ile bir olmayı isterler ve Allah’tan (hiçbir) söz gizleyemezler
**(43)** ey inanan(lar) [imanlı] kimseler siz sarhoşken namaza yaklaşmayın ki ne dediğinizi bilesiniz ve yoldan geçici olmanız dışında cünüp iken yıkanıncaya kadar (namaza yaklaşmayın) eğer hasta yahut yolculuk üzerinde iseniz yahut sizden biriniz tuvaletten gelmişse yahut kadınlara dokunmuşsanız su bulamadığınız takdirde temiz toprağa teyemmüm edin yüzlerinize ve ellerinize sürün şüphesiz [Afüv Ğafur] Allah çok affedendir çok bağışlayandır
**(44)** Kitaptan kendilerine bir pay verilen kimselerin [durumunu] görmedin mi? sapıklığı satın alıyorlar ve sizin yolu sapıtmanızı istiyorlar
**(45)** Allah sizin düşmanlarınızı daha iyi bilir Allah dost olarak yeter Allah yardımcı olarak yeter
**(46)** Yahudilerden öyleleri var ki kelimeleri yerlerinden kaydırıyorlar ve işittik ve isyan ettik ve dinlemez olası dinle ve dini taşlayarak ve dillerini eğip bükerek "ra’ina" [bizi güt] diyorlar (eğer) keşke onlar işittik ve ita’at ettik ve dinle ve bak bize [gör] deselerdi elbette kendileri için daha iyi ve daha sağlam olurdu fakat inkarlarından dolayı Allah onları la’netlemiştir pek azı hariç inanmazlar
**(47)** ey kimseler Kitap verilen(ler) cumartesi adamlarını la’netlediğimiz gibi onları da la’netlememizden ya da bazı yüzleri biz silip arkaları üzerine döndürmemizden önce yanınızdakini doğrulayıcı olarak indirdiğimiz şeye (Kur’ana)inanın Allah’ın buyruğu yapılır
**(48)** şüphesiz Allah kendisine ortak koşulmasını bağışlamaz ve dilediği kimseden bundan başkasını bağışlar ve Allah’a ortak koşan kimse gerçekten büyük bir günah [olan yalanı] iftira etmiştir
**(49)** şu kendilerini övüp yüceltenleri görmedin mi? Hayır, ancak Allah dilediğini yüceltir onlara [en ufak] kıl kadar zulmedilmez
**(50)** Allah’a karşı bak nasıl yalan uyduruyorlar ve apaçık bir günah olarak bu (onlara) yeter
**(51)** Kitaptan kendilerine bir pay verilenleri görmedin mi? [şeytan putundan] cibt’e ve tağut’a inanıyorlar ve inkar edenler için bunlar inanan(lar)[dan imanlı] kimselerden daha doğru yolda(dırlar) diyorlar
**(52)** işte onlar Allah’ın la’netlediği (insanlardır) Allah kimi la’netlerse onun için artık (hiçbir) yardımcı bulamazsın
**(53)** yoksa onların mülkten bir payı var mı? öyle olsaydı insanlara bir çekirdek zerresi bile vermezlerdi
**(54)** yoksa Allah’ın lütfundan verdiği şeyi (vahiyleri) yüzünden insanlara [verilenleri] kıskanıyorlar mı oysa İbrahim soyuna Kitabı ve hikmeti biz verdik ve onlara büyük bir mülk verdik
**(55)** onlardan kimi O(Hak Kitabı)na inandı onlardan kimi de ondan yüz çevirdi öylesine de çılgın alevli cehennem yetti
**(56)** şüphesiz ayetlerimizi inkar eden(leri) [kafir] kimseleri yakında bir ateşe sokacağız derileri her piştikçe derileri azabı tadsınlar diye başkasıyla değiştireceğiz şüphesiz [Azîz Hakîm] Allah daima üstündür hüküm ve hikmet sahibidir
**(57)** inanan kimseleri ve iyi işler yapanları altlarından ırmaklar akan cennetlere sokacağız orada sürekli kalacaklardır orada kendilerine tertemiz eşler de vardır ve onları (hiç güneş sızmayan) eşsiz bir gölgeye sokacağız
**(58)** şüphesiz Allah size emanetleri ehline vermenizi ve insanlar arasında hükmettiğiniz zaman adaletle hükmetmenizi emreder şüphesiz Allah onunla size ne güzel öğüt veriyor doğrusu  [Semî Basîr] Allah işitendir görendir
**(59)** ey iman eden(ler) [imanlı] kimseler Allah’a ita’at edin ve Elçiye ve sizden olan buyruk sahibine ita’at edin herhangi bir şey hakkında eğer anlaşmazlığa düşerseniz Allah’a ve Elçiye onu götürün eğer Allah’a ve ahiret gününe inanıyor iseniz sonuç bakımından da bu daha iyidir ve daha güzeldir
**(60)** sana indirilene şeylere ve senden önce indirilene şeylere sadece kendilerinin inandıklarını zanneden(leri) kimseleri görmedin mi hakem olarak tağuta başvurmak istiyorlar oysa onu inkar etmeleri emredilmişti ve Şeytan da iyice sapkınlıkla onları saptırmak istiyor
**(61)** kendilerine Allah’ın indirdiği(ne) şeye ve Elçiye gelin dendiği zaman o ikiyüzlülerin senden büsbütün uzaklaşmakla uzaklaştıklarını görürsün
**(62)** ne zaman ki nasıl elleriyle yaptıkları (kötülükler) yüzünden başlarına bir felaket gelince sonra hemen sana gelirler biz sadece iyilik etmek ve uzlaştırmak istedik diye Allah’a yemin ederler 
**(63)** işte onlar ki Allah onların kalblerinde olanı bilir onlara aldırma ve onlara öğüt ver ve onların içlerine işleyecek güzel bir söz söyle
**(64)** hiçbir elçiyi biz Allah’ın izniyle ita’at edilmekten başka bir amaçla göndermedik eğer onlar kendilerine zulmettikleri zaman sana gelseler Allah’tan bağışlanma dileseler ve Elçi onların bağışlanmasını dileseydi elbette [Tevvâb Rahîm] Allah’ı merhametli affedici bulurlardı
**(65)** Rabbin hakkı için hayır aralarında çıkan çekişmeli işlerde seni hakem yaparak sonra da senin verdiğin hükme kendilerinin içlerinde bir burukluk bulunmadan ve tam bir teslimiyetle teslim olmadıkça inanmış olmazlar 
**(66)** ve eğer biz onlara kendinizi öldürün ya da yurtlarınızdan çıkın yazsaydık içlerinden pek azı hariç bunu yapmazlardı eğer onlar kendilerine öğütlenen şeyi yapsalardı kendileri için elbette daha iyi ve sağlamlıkta daha sağlam olurdu
**(67)** ve o zaman kendilerine katımızdan büyük bir mükafat verirdik
**(68)** ve onları doğru bir yola [hidayete] iletirdik
**(69)** ve kim Allah’a ve Elçi’ye ita’at ederse işte onlar Allah’ın kendilerini ni’metlendirdiği kimselerle; [nebi] peygamberlerle ve sıddiklarla ve şehidlerle ve Salihlerle beraberdir ve onlar ne güzel arkadaştır
**(70)** bu ni’met Allah’tandır ve [Alîm] Allah bilen olarak yeter
**(71)** ey inanan(lar); [imanlı] kimseler korunma(tedbirleri)nizi alın bölük bölük savaşa gidin ya da hep birlikte savaşa gidin
**(72)** ve şüphesiz içinizden bir kısmı var ki pek ağır davranır eğer size bir felaket erişirse der ki muhakkak Allah bana lütfetti onlarla beraber hazır bulunmadım 
**(73)** ve eğer size Allah’tan bir ni’met erişirse sanki sizinle kendisi arasında hiç sevgi yokmuş gibi der keşke ben de onlarla beraber olsaydım büyük bir başarı kazansaydım
**(74)** ahireti karşılığında dünya hayatını satan(lar); [o] kimseler Allah yolunda savaşsınlar ve kim savaşır da Allah yolunda öldürülür veya galib gelirse yakında biz ona büyük bir mükafat vereceğiz
**(75)** size ne oldu? Allah yolunda ve zayıf erkekler ve kadınlar ve çocuklar (uğrunda) savaşmıyorsunuz [o] kimseler Rabbimiz halkı zalim şu kentten bizi çıkar ve bize katından bir koruyucu ver ve bize katından bir yardımcı ver diyorlar
**(76)** inanan(lar); [imanlı] kimseler Allah yolunda savaşırlar ve inkar eden(ler); [kafir] kimseler tağut [dindar olmayan azgın ve isyankar] yolunda savaşırlar o halde şeytanın dostlarıyle savaşın şüphesiz şeytanın hilesi zayıftır
**(77)** kendilerine ellerinizi (savaştan) çekin ve namazı kılın ve zekatı verin denilen(leri); [o] kimseleri görmedin mi kendilerine savaş yazılıdığı zaman içlerinden Allah’tan korkar gibi hatta daha fazla korkuyla bir grup insanlardan korkmaya başladılar ve dediler ki Rabbimiz niçin bize savaş yazdın keşke bizi yakın bir süreye kadar erteleseydin de ki dünya geçimi azdır ve korunan kimse için ahiret daha iyidir size kıl kadar haksızlık edilmez
**(78)** nerede olsanız ve eğer sağlam kaleler içinde bulunsanız ölüm yine sizi bulur ve eğer onlara bir iyilik erişirse bu Allah tarafındandır eğer onlara bir kötülük erişirse bu senin yüzündendir derler de ki hepsi Allah tarafındandır ne oluyor ki bu topluma söz anlamaya yanaşmıyorlar
**(79)** sana gelen şey her iyilik Allah’tandır ve sana gelen şey her kötülük kendi(günahın yüzü)ndendir ve seni insanlara elçi [Rasûl] gönderdik ve Allah şahid olarak yeter
**(80)** kim Elçi’ye ita’at ederse muhakkak ki Allah’a ita’at etmiş olur kim de yüz çevirirse biz seni onların üzerine bekçi göndermedik
**(81)** 
**(82)** 
**(83)** 
**(84)** 
**(85)** 
**(86)** 
**(87)** 
**(88)** 
**(89)** 
**(90)** 
**(91)**
**(92)** 
**(93)** 
**(94)** 
**(95)** 
**(96)** 
**(97)** 
**(98)** 
**(99)** 
**(100)** 
**(101)** 
**(102)** 
**(103)** 
**(104)** 
**(105)** 
**(106)** 
**(107)** 
**(108)** 
**(109)** 
**(110)** 
**(111)** 
**(112)** 
**(113)** 
**(114)** 
**(115)** 
**(116)** 
**(117)** 
**(118)** 
**(119)** 
**(120)** 
**(121)** 
**(122)** 
**(123)** 
**(124)** 
**(125)** 
**(126)** 
**(127)** 
**(128)** 
**(129)** 
**(130)** 
**(131)** 
**(132)** 
**(133)** 
**(134)** 
**(135)** 
**(140)** 
**(141)** 
**(142)** 
**(143)** 
**(144)** 
**(145)** 
**(146)** 
**(147)** 
**(148)** 
**(149)** 
**(150)** 
**(151)** 
**(152)** 
**(153)** 
**(154)** 
**(155)** 
**(156)** 
**(157)** 
**(158)** 
**(159)** 
**(160)** 
**(161)** 
**(162)** 
**(163)** 
**(164)** 
**(165)** 
**(166)** 
**(167)** 
**(168)** 
**(169)** 
**(170)** 
**(171)** 
**(172)** 
**(173)** 
**(174)** 
**(175)** 
**(176)** 


![04Nisâ-Medeni]({{ site.baseurl }}/assets/images/ayrac-muhur.png "mühür")
