---
layout: page
title: Alak
description: Zahmet, meşakkat gidermek, yapışkan ve ilişkin nesne, muhabbet eylemek
excerpt: "96'ncı Sûre"
modified: 2017-08-29T17:50:00.564948 17:00
encoding: UTF-8
tag: 
 - Alak
---

## 96. Alak Sûresi
**(1)** yaratan Rabbinin adıyla oku
**(2)** alaktan insanı O yarattı
**(3)** oku ve Rabbin en büyük kerem sahibidir
**(4)** O ki kalemle öğretti
**(5)** bilmediği şeyi insana öğretti
**(6)** hayır şüphesiz insan azar
**(7)** kendini zengin (kendine yeterli) gördüğü için
**(8)** şüphesiz dönüş Rabbinedir
**(9)** gördün mü? şu men’edeni
**(10)** namaz kıldığı zaman bir kulu?
**(11)** gördün mü? ya doğru yol [hidayet] üzerinde olursa
**(12)** yahut [kötülükten] korunmayı emredese
**(13)** gördün mü? ya yalanlarsa? ve yüz çevirirse? 
**(14)** muhakkak Allah'ın gördüğünü bilmedi mi (o)?
**(15)** hayır eğer bundan vazgeçmezse perçeminde mutlakan yakalarız
**(16)** günahkar yalancı perçem(den)
**(17)** o zaman meclisini çağırsın
**(18)** biz de zebanileri çağıracağız
**(19)** hayır ona [gönülden itaat etme] boyun eğme, secde et ve yaklaş

![96]({{ site.baseurl }}/assets/images/ayrac-muhur.png)
