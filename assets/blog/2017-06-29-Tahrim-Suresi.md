---
layout: page
title: Tahrîm
description: Sûre
excerpt: "66'ncı Sûre"
modified: 2017-08-29T17:50:00.564948 17:00
encoding: UTF-8
tag: 
 - Tahrîm
---

## 66. Tahrîm Sûresi

**(1)** Ey peygamber, Allah’ın sana helal kıldığı şeyi eşlerinin hatırını isteyerek niçin? haram kılıyorsun. Allah bağışlayadır, esirgeyendir.
**(2)** Andolsun, Allah size yeminlerinizi çözmeyi meşru’ kılmıştır ve Allah sizin sahibinizdir ve O bilendir, hüküm ve hikmet sahibidir.
**(3)** Ve hani peygamber, eşlerinden birine gizlice bir söz söylemişti, ne zaman ki onu (sözü) (eşi) haber verdi ve Allah ona (peypambere) onu muttali kıldı, onun bir kısmını bildirmişti ve bir kısmından da vazgeçmişti, ne zaman ki, bunu eşine haber verince (eşi) dedi bunu sana kim? söyledi, dedi ki, bana bilen, haber alan söyledi.
**(4)** Eğer kalblerinizin sapmış olmasından dolayı Allah’a ikiniz tevbe ederseniz ve eğer ona karşı birbirinize arka olursanız, şüphesiz, Allah’tır O, onun koruyucusu ve Cibril’dir ve mü’minlerin iyileridir ve sonra melekler bundan ona arkadır.
**(5)** Belki de, onun Rabbi, eğer o sizi boşarsa, inanan, gönülden ita’at eden, tevbe eden, ibadet eden, seyahat eden, (kendisini Allah’a) teslim eden, sizden daha hayırlı, dul ve bakire eşlerle onu değiştirir 
**(6)** Ey inanan(lar) kimseler, kendinizi ve ailenizi bir ateşten koruyun, onun yakıtı ise, insanlardır ve taşlardır, onun başında Allah’ın kendilerine buyurduğu şeye karşı gelmeyen ve emredildikleri şeyi yapan gayet katı, şiddetli melekler vardır.
**(7)** Ey inkar eden(ler) kimseler, bugün özür dilemeyin çünkü ancak siz yapıyor(lar) olduğunuz şeylerle cezalandırılıyorsunuz.
**(8)** Ey inanan(lar) kimseler, Allah’a yürekten tevbe ile tevbe edin, umulur ki, Rabbiniz sizden kötülüklerinizi örter, sizi altlarından ırmaklar akan cennetlere sokar, Allah’ın peygamberi ve onunla beraber inanmış olanları utandırmayacağı günde onların nuru önlerinden,  önlerinden ve sağ yanlarından koşar, derler ki, Rabbimiz bize nurumuzu tamamla ve bizi bağışla doğrusu senin her şey üzerine gücün yeter.
**(9)** Ey O! Peygamber kafirlerle ve münafıklarla cihad et ve onlara karşı katı davran, onların varacağı yer cehennemdir, ne kötü varılacak yerdir.
**(10)** Allah inkar eden(ler) kimseler için, misal ile anlattı, Nuh’un karısını ve Lut’un karısını bu ikisi salih kullarımızdan iki kulun (nikahı) altında idiler fakat ihanet ettiler, (kocaları) Allahtan(hiçbir) şeyi onlardan savamadı ve haydi girenlerle beraber ateşe girin denildi. 
**(11)** Ve Allah inananlar hakkında misal ile Fir’avn’ın karısını anlattı, Rabbim bana katında cennetin içinde bir ev yap demişti hani ve  Fir’avndan ve onun (kötü) işinden beni kurtar ve zalimler topluluğundan beni kurtar.
**(12)** Ve İmran’ın kızı Meryem’i, O ırzını korumuştu, biz de ona ruhumuzdan üflemiştik ve Rabbinin kelimelerini ve Kitaplarını doğrulamıştı ve gönülden ita’at edenlerden olmuştu. 

![66]({{ site.baseurl }}/assets/images/ayrac-muhur.png)
